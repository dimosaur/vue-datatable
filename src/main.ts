import { createApp, ComponentCustomProperties } from 'vue';
import { Store } from 'vuex';

import { RootState } from '@/store/types';
import store from './store';
import App from './App.vue';

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $store: Store<RootState>;
  }
}

createApp(App).use(store).mount('#app');
