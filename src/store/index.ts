import { createStore } from 'vuex';
import { RootState, Item } from '@/store/types';

import Storage from '@/store/storage';

import data from '@/data.json';
import { actions, mutations } from '@/store/constants';

const store = createStore<RootState>({
  state: (): RootState => ({
    titles: {},
    items: [],
  }),
  mutations: {
    [mutations.TITLES](state: RootState, titles: Item) {
      state.titles = { ...titles };
    },
    [mutations.ITEMS](state: RootState, items: Item[]) {
      state.items = [...items];
    },
    [mutations.ITEM](state: RootState, [index, item]: [number, Item]) {
      state.items = [
        ...state.items.slice(0, index),
        item,
        ...state.items.slice(index + 1),
      ];
    },
  },
  actions: {
    async [actions.INITIATE_STORE](context) {
      Storage.getTitles().then(async (titles) => {
        context.commit(mutations.TITLES, titles);
        const items = await Storage.getItems();
        context.commit(mutations.ITEMS, items);
      }).catch((error) => {
        if (error.message === '404') {
          context.dispatch(actions.API_CALL).then((responseData: RootState) => {
            Storage.setTitles(responseData.titles);
            Storage.setItems(responseData.items);
          });
        }
      });
    },
    [actions.API_CALL](context) {
      return new Promise<RootState>((resolve) => {
        resolve(data);
      }).then((responseData) => {
        console.log(actions.API_CALL);
        context.commit(mutations.TITLES, responseData.titles);
        context.commit(mutations.ITEMS, responseData.items);
        return responseData;
      });
    },
    [actions.MUTATE_ITEM](context, [index, item]: [number, Item]) {
      Storage.mutateItem(item, index);
      context.commit(mutations.ITEM, [index, item]);
    },
  },
  getters: {
    titles: (state): Item => state.titles,
    items: (state): Item[] => state.items,
  },
});

export default store;
