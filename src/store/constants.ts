export const actions = {
  INITIATE_STORE: 'INITIATE_STORE',
  API_CALL: 'API_CALL',
  MUTATE_ITEM: '',
};

export const mutations = {
  TITLES: 'TITLES',
  ITEMS: 'ITEMS',
  ITEM: 'ITEM',
};
