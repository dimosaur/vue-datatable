import { Item } from '@/store/types';

export default class Storage {
  static initDB(): Promise<IDBObjectStore> {
    return new Promise<IDBObjectStore>((resolve, reject) => {
      const request = window.indexedDB.open('datatable');
      request.onupgradeneeded = () => {
        const db = request.result;
        const store = db.createObjectStore('items');
        resolve(store);
      };
      request.onsuccess = () => {
        const db = request.result;
        const store = db.transaction('items', 'readwrite').objectStore('items');
        resolve(store);
      };
      request.onerror = () => {
        reject(request.result);
      };
    });
  }

  static async getItemsDB(): Promise<Item[]> {
    const store: IDBObjectStore = await Storage.initDB();

    return new Promise<Item[]>((resolve, reject) => {
      store.getAll(IDBKeyRange.lowerBound(0, true)).onsuccess = (event) => {
        const items = (event.target as IDBRequest).result;
        if (items.length) {
          resolve(items as Item[]);
        } else {
          reject(new Error('404'));
        }
      };
    });
  }

  static async setItemsDB(items: Item[]) {
    const store: IDBObjectStore = await Storage.initDB();

    items.forEach((item, i) => {
      store.add(item, i + 1);
    });
  }

  static async getTitlesDB(): Promise<Item> {
    const store: IDBObjectStore = await Storage.initDB();

    return new Promise<Item>((resolve, reject) => {
      store.get(0).onsuccess = (event) => {
        const titles = (event.target as IDBRequest).result;
        if (titles === undefined) {
          reject(new Error('404'));
        } else {
          resolve(titles as Item);
        }
      };
    });
  }

  static async setTitlesDB(titles: Item) {
    const store: IDBObjectStore = await Storage.initDB();
    store.add(titles, 0);
  }

  static async mutateItemDB(item: Item, index: number) {
    const store: IDBObjectStore = await Storage.initDB();
    store.put(item, index + 1);
  }

  static async getItemsLS(): Promise<Item[]> {
    return new Promise((resolve, reject) => {
      const json = localStorage.getItem('items');
      if (json) {
        const items = JSON.parse(json) as Item[];
        resolve(items);
      } else {
        reject(new Error('404'));
      }
    });
  }

  static async setItemsLS(items: Item[]) {
    const json = JSON.stringify(items);
    localStorage.setItem('items', json);
  }

  static async getTitlesLS(): Promise<Item> {
    return new Promise((resolve, reject) => {
      const json = localStorage.getItem('titles');
      if (json) {
        const item = JSON.parse(json) as Item;
        resolve(item);
      } else {
        reject(new Error('404'));
      }
    });
  }

  static async setTitlesLS(titles: Item) {
    const json = JSON.stringify(titles);
    localStorage.setItem('titles', json);
  }

  static async mutateItemLS(item: Item, index: number) {
    const items = await Storage.getItems() as Item[];
    items[index] = item;
    Storage.setItemsLS(items);
  }

  static getItems(): Promise<Item[]> {
    if (window.indexedDB) {
      return Storage.getItemsDB();
    }
    return Storage.getItemsLS();
  }

  static setItems(items: Item[]) {
    if (window.indexedDB) {
      Storage.setItemsDB(items);
    } else {
      Storage.setItemsLS(items);
    }
  }

  static getTitles(): Promise<Item> {
    if (window.indexedDB) {
      return Storage.getTitlesDB();
    }
    return Storage.getTitlesLS();
  }

  static setTitles(titles: Item) {
    if (window.indexedDB) {
      Storage.setTitlesDB(titles);
    } else {
      Storage.setTitlesLS(titles);
    }
  }

  static mutateItem(item: Item, index: number) {
    if (window.indexedDB) {
      Storage.mutateItemDB(item, index);
    } else {
      Storage.mutateItemLS(item, index);
    }
  }
}
